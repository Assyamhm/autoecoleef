﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoecole
{
    public partial class FrmEleve : Form
    {
        private autoecoleEntities mesDonneesEF;
        public FrmEleve(autoecoleEntities mesDonneesEF)
        {
            InitializeComponent();
            for (int i = 0; i < 30; i++)
                this.cmbCredit.Items.Add(i);
            this.mesDonneesEF = mesDonneesEF;
            this.bdgEleve.DataSource = this.mesDonneesEF.eleves;
          
        }
        private int getNumEleve()
        {
            var reqDernier = (from el in this.mesDonneesEF.eleves
                              orderby el.id descending
                              select  el );
            eleve dernierEleve = reqDernier.First();
            int n = dernierEleve.id + 1;
            return n;
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
          
          
        }

        

        private void bindingNavigatorAddNewItem_Click_1(object sender, EventArgs e)
        {
            this.txtNum.Text = this.getNumEleve().ToString();
            
          //  bindingNavigatorAddNewItem.Enabled = false;
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (txtAdresse.Text == "")
                MessageBox.Show("pas ok");
        }

        private void bindingNavigatorAddNewItem_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void bindingNavigatorAddNewItem_CheckStateChanged(object sender, EventArgs e)
        {
            if (txtAdresse.Text == "")
            {
                MessageBox.Show("Il faut remplir les champ du nouvel élève");
                bdgEleve.CancelEdit();// permet d'annuler l'ajout dans le cas ou le nouvel élève n'a pas d'info

            }
        }
        // on a mis la propriété chekOnclick à true afin de pouvoir intercepter lorsque le bouton est checké

        private void bindingNavigatorMovePreviousItem_CheckedChanged(object sender, EventArgs e)
        {
            if (txtAdresse.Text == "" || txtNom.Text == "" || txtPrenom.Text == "")
            {
                MessageBox.Show("Il faut remplir tous les champs du nouvel élève");
                bdgEleve.CancelEdit();// permet d'annuler l'ajout dans le cas 
                                        // où le nouvel élève n'a pas d'info
            }
        }

    }
}
