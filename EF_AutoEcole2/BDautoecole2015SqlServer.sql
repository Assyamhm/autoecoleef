

CREATE TABLE eleve (
  id int(11) NOT NULL,
  nom varchar(20) DEFAULT NULL,
  dateInscription date DEFAULT NULL,
  prenom varchar(20) DEFAULT NULL,
  adresse varchar(30) DEFAULT NULL,
  creditHoraire int(2) DEFAULT NULL,
  PRIMARY KEY (id)
) 


INSERT INTO eleve (id, nom, dateInscription, prenom, adresse, creditHoraire) VALUES(12, 'Trépert', '2014-05-11', 'Annietyt', '93600 Aulnay', 17);
INSERT INTO eleve (id, nom, dateInscription, prenom, adresse, creditHoraire) VALUES(21, 'Pirounet', '2014-03-10', 'France', '93600 Aulnay', 19);
INSERT INTO eleve (id, nom, dateInscription, prenom, adresse, creditHoraire) VALUES(23, 'Trépert', '2014-01-15', 'Marc', '93100 Montreuil', 4);
INSERT INTO eleve (id, nom, dateInscription, prenom, adresse, creditHoraire) VALUES(36, 'Gamzi', '2014-06-21', 'Christophe', '93100 Montreuil', 18);
INSERT INTO eleve (id, nom, dateInscription, prenom, adresse, creditHoraire) VALUES(37, 'Chaballe', '2014-05-09', 'Marc', '21 rue Bareau 93100 Montreuil', 30);
INSERT INTO eleve (id, nom, dateInscription, prenom, adresse, creditHoraire) VALUES(45, 'Poireau', '2014-08-19', 'Gilles', '93000 Bobigny', 18);
INSERT INTO eleve (id, nom, dateInscription, prenom, adresse, creditHoraire) VALUES(46, 'Ardi', '2014-01-14', 'Jean', '45 rue Petit 75019', 30);


CREATE TABLE lecon (
  id int(11) NOT NULL,
  `date` date NOT NULL,
  idEleve int(11) DEFAULT NULL,
  heure int(11) DEFAULT NULL,
  duree int(11) DEFAULT NULL,
  effectueeOui_Non tinyint(1) NOT NULL,
  numImmaVehicule varchar(8) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY numImmaVehicule (numImmaVehicule),
  KEY idEleve (idEleve)
) 


INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(1021, '2014-09-11', 12, 12, 1, 1, '1123YA93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(1435, '2014-05-09', 23, 12, 1, 1, '1123YA93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(1512, '2014-09-14', 23, 19, 2, 1, '129ABA93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(2125, '2014-09-12', 12, 17, 1, 1, '1123YA93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(2239, '2014-05-09', 36, 18, 1, 1, '4561WK93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(2777, '2014-09-14', 23, 16, 1, 1, '129ABA93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(2834, '2014-05-09', 12, 12, 1, 1, '1123YA93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(3397, '2014-05-09', 23, 11, 1, 1, '7891WJ93');
INSERT INTO lecon (id, `date`, idEleve, heure, duree, effectueeOui_Non, numImmaVehicule) VALUES(3562, '2014-05-09', 45, 15, 2, 1, '7891WJ93');


CREATE TABLE vehicule (
  numImma varchar(8) NOT NULL,
  modele varchar(20) DEFAULT NULL,
  couleur varchar(30) DEFAULT NULL,
  enEtat tinyint(1) NOT NULL,
  PRIMARY KEY (numImma)
) 


INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('1123YA93', 'Twingo', 'rouge', 1);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('1235YB93', 'Twingo', 'blanche', 1);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('129ABA93', 'Laguna', 'blanche', 1);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('1745ZE93', 'Mégane', 'Noire', 1);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('1845TY93', 'Mégane', 'Verte', 0);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('2289DR93', 'Peugeot 204', 'Blanche', 1);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('4561WK93', 'Twingo', 'beige', 1);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('457ABC93', 'Clio', 'bleue', 1);
INSERT INTO vehicule (numImma, modele, couleur, enEtat) VALUES('7891WJ93', 'Clio', 'noire', 1);

ALTER TABLE `lecon`
  ADD CONSTRAINT lecon_ibfk_1 FOREIGN KEY (numImmaVehicule) REFERENCES vehicule (numImma),
  ADD CONSTRAINT lecon_ibfk_2 FOREIGN KEY (idEleve) REFERENCES eleve (id);

