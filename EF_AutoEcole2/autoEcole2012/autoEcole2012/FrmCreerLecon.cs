﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmCreerLecon : Form
    {
        private autoecole2012Entities autoEcoleEntities;
        public FrmCreerLecon(autoecole2012Entities autoEcoleEntities)
        {
            InitializeComponent();
            this.autoEcoleEntities = autoEcoleEntities;
            this.bdgsEleve.DataSource = this.autoEcoleEntities.eleve;
            for (int i = 8; i < 20; i++)
                this.cmbHeure.Items.Add(i);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.autoEcoleEntities.lecon.AddObject(nouvellelecon());
            this.autoEcoleEntities.SaveChanges();


        }
        private lecon nouvellelecon()
        {
            int n = newNumLecon();
            //   int idEleve = Convert.ToInt32(cmbEleve.SelectedValue);
            eleve el = ((eleve)cmbEleve.SelectedValue);
            DateTime d = dTPdateLecon.Value;
            int heureLecon = Convert.ToInt32(this.cmbHeure.SelectedItem);
            int nbHeures = 1;
            if (this.radioButton2.Checked)
                nbHeures = 2;
            lecon l = new lecon();
            l.date = d;
            l.heure = heureLecon;
            l.duree = nbHeures;
            l.id = n;
            l.eleve = el;
            return l;
        }
        private int newNumLecon()
        {
            int n;
            var reqDernier = (from ra in this.autoEcoleEntities.lecon
                              orderby ra.id descending
                              select ra);
            lecon derniereLecon = reqDernier.First();
             n = derniereLecon.id + 1;

            return n;
        }
    }
}
