﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmAccueil : Form
    {
        private autoecole2012Entities autoEcoleEntities;
        public FrmAccueil()
        {
            InitializeComponent();
            this.autoEcoleEntities = new autoecole2012Entities();
            
        }

       

        private void gérerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEleve f = new FrmEleve(this.autoEcoleEntities);
            f.Show();
        }

        private void gérerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmVehicule f = new FrmVehicule(this.autoEcoleEntities);
            f.Show();
        }

        private void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmListeVehicules f = new FrmListeVehicules(this.autoEcoleEntities);
            f.Show();
        }

        private void gérerToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FrmCreerLecon f = new FrmCreerLecon(this.autoEcoleEntities);
            f.Show();
        }

        private void validerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmValiderLecon f = new FrmValiderLecon(this.autoEcoleEntities);
            f.Show();
        }
    }
}
