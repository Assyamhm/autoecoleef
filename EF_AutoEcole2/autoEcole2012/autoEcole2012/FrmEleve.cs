﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmEleve : Form
    {
        private autoecole2012Entities autoEcoleEntities;
        public FrmEleve(autoecole2012Entities autoEcoleEntities)
        {
            InitializeComponent();
            this.autoEcoleEntities = autoEcoleEntities;
            this.bdS.DataSource = this.autoEcoleEntities.eleve;
            for (int i = 1; i < 30; i++)
                this.cbxHoraire.Items.Add(i);
           
        }
        private int getNumEleve()
        {
            var reqDernier = (from el in this.autoEcoleEntities.eleve
                              orderby el.id descending
                              select el);
            eleve dernierEleve = reqDernier.First();
            int n = dernierEleve.id + 1;
            return n;

        }
        private void cbxHoraire_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

            txtNum.Text = this.getNumEleve().ToString();
            this.bdS.EndEdit();
            autoEcoleEntities.SaveChanges();
         }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            bdS.EndEdit();
            autoEcoleEntities.SaveChanges();

        }

        private void bindingNavigatorAddNewItem_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("cheked");
        }
    }
}
