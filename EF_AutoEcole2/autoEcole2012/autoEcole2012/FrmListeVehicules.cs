﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmListeVehicules : Form
    {
        private autoecole2012Entities autoEcoleEntities;
        public FrmListeVehicules(autoecole2012Entities autoEcoleEntities)
        {
            InitializeComponent();
            this.autoEcoleEntities = autoEcoleEntities;
            this.dGViewVehicules.DataSource = autoEcoleEntities.vehicule;
        }

        private void btnSauvegarder_Click(object sender, EventArgs e)
        {
            autoEcoleEntities.SaveChanges();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
             
        }
    }
}
