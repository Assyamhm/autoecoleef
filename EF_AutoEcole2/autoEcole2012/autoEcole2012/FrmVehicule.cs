﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmVehicule : Form
    {
        private autoecole2012Entities autoEcoleEntities;
        public FrmVehicule(autoecole2012Entities autoEcoleEntities)
        {
            InitializeComponent();
            this.autoEcoleEntities = autoEcoleEntities;
             
        }

        private void FrmEleve_Load(object sender, EventArgs e)
        {
            bs.DataSource = this.autoEcoleEntities.vehicule;

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            this.champsReadOnly(false);
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            this.champsReadOnly(true);
        }
        private void champsReadOnly(bool ok)
        {
            this.txtNumImma.ReadOnly = ok;
            this.txtCouleur.ReadOnly = ok;
            this.txtModele.ReadOnly = ok;
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            this.champsReadOnly(true);
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            this.champsReadOnly(true);
        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            this.champsReadOnly(true);
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            this.champsReadOnly(true);
        
        }

        private void vehiculeBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            autoEcoleEntities.SaveChanges();
        }
    }
}
