CREATE PROCEDURE creer_vehicule2
     (
        IN  p_id             VARCHAR(8), 
        IN  p_modele         VARCHAR(20), 
        IN  p_couleur        VARCHAR(30), 
        IN  p_etat           TINYINT(1)   
    )
BEGIN 
    INSERT INTO vehicule
    VALUES 
         ( 
           p_id, 
           p_modele, 
           p_couleur, 
           p_etat                       
         ) ; 
END|



