﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmMenu : Form
    {
        private  autoecoleEntities mesDonnesEF;
        public FrmMenu()
        {
            InitializeComponent();
            this.mesDonnesEF = new  autoecoleEntities();
          }

        private void gérerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEleve f = new FrmEleve(this.mesDonnesEF);
            f.Show(); 
        }

        private void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmListeVehicule f = new FrmListeVehicule( mesDonnesEF);
            f.Show();
        }

        private void gérerToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FrmAjoutLecon f = new FrmAjoutLecon(this.mesDonnesEF);
            f.Show();

        }

        private void validerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmValiderLecon f = new FrmValiderLecon(this.mesDonnesEF);
            f.Show();
        }

        private void validerTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmValiderLeconTest f = new FrmValiderLeconTest(this.mesDonnesEF);
            f.Show();
        }

        private void validerTest2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmValiderLeconTest2 f = new FrmValiderLeconTest2(this.mesDonnesEF);
            f.Show();
        }

        private void validerTest3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmvaliderLeconTest3 f = new FrmvaliderLeconTest3(this.mesDonnesEF);
            f.Show();
        }

        private void validerTest4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
             FrmvaliderLeconTest4 f = new FrmvaliderLeconTest4(this.mesDonnesEF);
            f.Show();
        }

        private void gérerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmVehicule f = new FrmVehicule(this.mesDonnesEF);
            f.Show();
        }

        private void testLinqToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormTestLINQ f = new FormTestLINQ(this.mesDonnesEF);
            f.Show();
        }

        private void listeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmListeLecon f = new frmListeLecon(this.mesDonnesEF);
            f.Show();
        }
        }

       

        
    }

