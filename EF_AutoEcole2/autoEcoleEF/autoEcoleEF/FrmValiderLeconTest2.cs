﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmValiderLeconTest2 : Form
    {
        private autoecoleEntities mesDonnesEF;
        public FrmValiderLeconTest2(autoecoleEntities mesDonnesEF)
        {
            InitializeComponent();
            this.mesDonnesEF = mesDonnesEF;
            this.bdgSeleve.DataSource = this.mesDonnesEF.eleves.Include("lecons");
            this.bdgSlecon.DataSource = this.mesDonnesEF.lecons;
            this.bdgSvehicule.DataSource = this.mesDonnesEF.vehicules;
         }
        private void btnEnregister_Click(object sender, EventArgs e)
        {
            this.bdgSeleve.EndEdit();
            this.bdgSlecon.EndEdit();
            this.bdgSvehicule.EndEdit();
            this.mesDonnesEF.SaveChanges();
        }
    }
}

