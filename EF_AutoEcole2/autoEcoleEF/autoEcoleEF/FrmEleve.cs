﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmEleve : Form
    {
        private autoecoleEntities mesDonneesEF;
        public FrmEleve(autoecoleEntities mesDonneesEF)
        {
            InitializeComponent();
            for (int i = 0; i < 30; i++)
                this.cmbCredit.Items.Add(i);
            this.mesDonneesEF = mesDonneesEF;
            this.bdgSourceEleve.DataSource = mesDonneesEF.eleves;
        }
        private int getNumEleve()
        {
            var reqDernier = (from el in this.mesDonneesEF.eleves
                              orderby el.id descending
                              select  el );
            eleve dernierEleve = reqDernier.First();
            int n = dernierEleve.id + 1;
            return n;
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            this.txtNum.Text  = this.getNumEleve().ToString();
            this.bdgSourceEleve.EndEdit();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.mesDonneesEF.SaveChanges();
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

        }

    }
}
