﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FormTestLINQ : Form
    {
        private autoecoleEntities mesDonnesEF;
        public FormTestLINQ(autoecoleEntities mesDonnesEF)
        {
            InitializeComponent();
            this.mesDonnesEF = mesDonnesEF;
            var req = from e in this.mesDonnesEF.eleves
                     group e by e.id into groupeEleve
                      join l in this.mesDonnesEF.lecons on groupeEleve.Key equals l.idEleve

                      select groupeEleve;

            var req2 = from l in this.mesDonnesEF.lecons
                     group l by l.idEleve into groupeLecon
                      join e in this.mesDonnesEF.eleves on groupeLecon.Key equals e.id
                       select new { nom = e.nom,totalHeure = groupeLecon.Sum(cumul => cumul.duree ) };

            this.dataGridView1.DataSource = req2;
        }
    }
}
