﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmValiderLeconTest : Form
    { 
        private autoecoleEntities mesDonnesEF;
        public FrmValiderLeconTest(autoecoleEntities mesDonnesEF)
        {
            InitializeComponent();
            this.mesDonnesEF = mesDonnesEF;
            this.leconDataGridView.AutoGenerateColumns = false;

            // création de la source de données
            var lesLecons = from l in mesDonnesEF.lecons
                         //   where l.effectueeOui_Non == false
                            select l;
                            
           // Chargement du composant de binding lecon par le résultat de la requête

            this.bdgSlecon.DataSource = lesLecons;

            //Chargement du composant de binding vehicule par le résultat de la requête
            this.bdgSvehicule.DataSource = mesDonnesEF.vehicules;

            // création d'une colonne de type text pour la date
            DataGridViewTextBoxColumn txtColonneDate = new DataGridViewTextBoxColumn();
            txtColonneDate.HeaderText = "date";
            txtColonneDate.Name = "date";
            txtColonneDate.DataPropertyName = "date";
            this.leconDataGridView.Columns.Add(txtColonneDate);

            // création d'une colonne de type text pour l'heure
            DataGridViewTextBoxColumn txtColonneHeure = new DataGridViewTextBoxColumn();
             txtColonneHeure.HeaderText = "heure";
             txtColonneHeure.Name = "heure";
             txtColonneHeure.DataPropertyName = "heure";
             this.leconDataGridView.Columns.Add(txtColonneHeure);

             // création d'une colonne de type combobox pour le numImma
            DataGridViewComboBoxColumn cmbColonneVehicule = new DataGridViewComboBoxColumn();
             cmbColonneVehicule.HeaderText = "vehicule";
             cmbColonneVehicule.Name = "vehicule";
             cmbColonneVehicule.DisplayMember = "numImma";
             cmbColonneVehicule.DataPropertyName = "numImmaVehicule";
             cmbColonneVehicule.DataSource = this.bdgSvehicule;
             this.leconDataGridView.Columns.Add(cmbColonneVehicule);

             // création d'une colonne de type checkBox pour effectuee_OuiNon
             DataGridViewCheckBoxColumn chkColonneOk = new DataGridViewCheckBoxColumn();
             chkColonneOk.HeaderText = "effectuee";
             chkColonneOk.Name = "effectuee";
             chkColonneOk.DataPropertyName = "effectueeOui_Non";
             this.leconDataGridView.Columns.Add(chkColonneOk);
             this.leconDataGridView.DataSource = this.bdgSlecon;

            // Binding des labels des nom et prénom 
            lblNom.DataBindings.Add("Text", bdgSlecon, "eleve.nom");
             lblPrenom.DataBindings.Add("Text", bdgSlecon, "eleve.prenom");
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            this.bdgSlecon.EndEdit();
            this.mesDonnesEF.SaveChanges();
        }
       
    }
}

