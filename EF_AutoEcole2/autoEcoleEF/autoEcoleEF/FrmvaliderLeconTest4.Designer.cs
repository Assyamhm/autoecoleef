﻿namespace autoEcoleEF
{
    partial class FrmvaliderLeconTest4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bdgSourceLecon = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idEleveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectueeOuiNonDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.eleveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehiculeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSourceLecon)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.idEleveDataGridViewTextBoxColumn,
            this.effectueeOuiNonDataGridViewCheckBoxColumn,
            this.eleveDataGridViewTextBoxColumn,
            this.vehiculeDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.bdgSourceLecon;
            this.dataGridView1.Location = new System.Drawing.Point(-20, 36);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(604, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // bdgSourceLecon
            // 
            this.bdgSourceLecon.DataSource = typeof(autoEcoleEF.lecon);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // idEleveDataGridViewTextBoxColumn
            // 
            this.idEleveDataGridViewTextBoxColumn.DataPropertyName = "idEleve";
            this.idEleveDataGridViewTextBoxColumn.HeaderText = "idEleve";
            this.idEleveDataGridViewTextBoxColumn.Name = "idEleveDataGridViewTextBoxColumn";
            // 
            // effectueeOuiNonDataGridViewCheckBoxColumn
            // 
            this.effectueeOuiNonDataGridViewCheckBoxColumn.DataPropertyName = "effectueeOui_Non";
            this.effectueeOuiNonDataGridViewCheckBoxColumn.HeaderText = "effectueeOui_Non";
            this.effectueeOuiNonDataGridViewCheckBoxColumn.Name = "effectueeOuiNonDataGridViewCheckBoxColumn";
            // 
            // eleveDataGridViewTextBoxColumn
            // 
            this.eleveDataGridViewTextBoxColumn.DataPropertyName = "eleve";
            this.eleveDataGridViewTextBoxColumn.HeaderText = "eleve";
            this.eleveDataGridViewTextBoxColumn.Name = "eleveDataGridViewTextBoxColumn";
            // 
            // vehiculeDataGridViewTextBoxColumn
            // 
            this.vehiculeDataGridViewTextBoxColumn.DataPropertyName = "vehicule";
            this.vehiculeDataGridViewTextBoxColumn.HeaderText = "vehicule";
            this.vehiculeDataGridViewTextBoxColumn.Name = "vehiculeDataGridViewTextBoxColumn";
            // 
            // FrmvaliderLeconTest4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 262);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FrmvaliderLeconTest4";
            this.Text = "FrmvaliderLeconTest4";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSourceLecon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bdgSourceLecon;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEleveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn effectueeOuiNonDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eleveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehiculeDataGridViewTextBoxColumn;
    }
}