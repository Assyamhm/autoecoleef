﻿namespace autoEcoleEF
{
    partial class FrmMenu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eleveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gérerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.véhiculeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gérerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leçonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gérerToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.validerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validerTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validerTest2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validerTest3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validerTest4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testLinqToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.eleveToolStripMenuItem,
            this.véhiculeToolStripMenuItem,
            this.leçonToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(382, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "&Fichier";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.quitterToolStripMenuItem.Text = "&Quitter";
            // 
            // eleveToolStripMenuItem
            // 
            this.eleveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gérerToolStripMenuItem});
            this.eleveToolStripMenuItem.Name = "eleveToolStripMenuItem";
            this.eleveToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.eleveToolStripMenuItem.Text = "&Elève";
            // 
            // gérerToolStripMenuItem
            // 
            this.gérerToolStripMenuItem.Name = "gérerToolStripMenuItem";
            this.gérerToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.gérerToolStripMenuItem.Text = "&Gérer";
            this.gérerToolStripMenuItem.Click += new System.EventHandler(this.gérerToolStripMenuItem_Click);
            // 
            // véhiculeToolStripMenuItem
            // 
            this.véhiculeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gérerToolStripMenuItem1,
            this.listeToolStripMenuItem});
            this.véhiculeToolStripMenuItem.Name = "véhiculeToolStripMenuItem";
            this.véhiculeToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.véhiculeToolStripMenuItem.Text = "&Véhicule";
            // 
            // gérerToolStripMenuItem1
            // 
            this.gérerToolStripMenuItem1.Name = "gérerToolStripMenuItem1";
            this.gérerToolStripMenuItem1.Size = new System.Drawing.Size(102, 22);
            this.gérerToolStripMenuItem1.Text = "&Gérer";
            this.gérerToolStripMenuItem1.Click += new System.EventHandler(this.gérerToolStripMenuItem1_Click);
            // 
            // listeToolStripMenuItem
            // 
            this.listeToolStripMenuItem.Name = "listeToolStripMenuItem";
            this.listeToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.listeToolStripMenuItem.Text = "&Liste";
            this.listeToolStripMenuItem.Click += new System.EventHandler(this.listeToolStripMenuItem_Click);
            // 
            // leçonToolStripMenuItem
            // 
            this.leçonToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gérerToolStripMenuItem2,
            this.validerToolStripMenuItem,
            this.validerTestToolStripMenuItem,
            this.validerTest2ToolStripMenuItem,
            this.validerTest3ToolStripMenuItem,
            this.validerTest4ToolStripMenuItem,
            this.testLinqToolStripMenuItem,
            this.listeToolStripMenuItem1});
            this.leçonToolStripMenuItem.Name = "leçonToolStripMenuItem";
            this.leçonToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.leçonToolStripMenuItem.Text = "&Leçon";
            // 
            // gérerToolStripMenuItem2
            // 
            this.gérerToolStripMenuItem2.Name = "gérerToolStripMenuItem2";
            this.gérerToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.gérerToolStripMenuItem2.Text = "&Ajouter";
            this.gérerToolStripMenuItem2.Click += new System.EventHandler(this.gérerToolStripMenuItem2_Click);
            // 
            // validerToolStripMenuItem
            // 
            this.validerToolStripMenuItem.Name = "validerToolStripMenuItem";
            this.validerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.validerToolStripMenuItem.Text = "&Valider";
            this.validerToolStripMenuItem.Click += new System.EventHandler(this.validerToolStripMenuItem_Click);
            // 
            // validerTestToolStripMenuItem
            // 
            this.validerTestToolStripMenuItem.Name = "validerTestToolStripMenuItem";
            this.validerTestToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.validerTestToolStripMenuItem.Text = "ValiderTest";
            this.validerTestToolStripMenuItem.Click += new System.EventHandler(this.validerTestToolStripMenuItem_Click);
            // 
            // validerTest2ToolStripMenuItem
            // 
            this.validerTest2ToolStripMenuItem.Name = "validerTest2ToolStripMenuItem";
            this.validerTest2ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.validerTest2ToolStripMenuItem.Text = "validerTest2";
            this.validerTest2ToolStripMenuItem.Click += new System.EventHandler(this.validerTest2ToolStripMenuItem_Click);
            // 
            // validerTest3ToolStripMenuItem
            // 
            this.validerTest3ToolStripMenuItem.Name = "validerTest3ToolStripMenuItem";
            this.validerTest3ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.validerTest3ToolStripMenuItem.Text = "validerTest3";
            this.validerTest3ToolStripMenuItem.Click += new System.EventHandler(this.validerTest3ToolStripMenuItem_Click);
            // 
            // validerTest4ToolStripMenuItem
            // 
            this.validerTest4ToolStripMenuItem.Name = "validerTest4ToolStripMenuItem";
            this.validerTest4ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.validerTest4ToolStripMenuItem.Text = "validerTest4";
            this.validerTest4ToolStripMenuItem.Click += new System.EventHandler(this.validerTest4ToolStripMenuItem_Click);
            // 
            // testLinqToolStripMenuItem
            // 
            this.testLinqToolStripMenuItem.Name = "testLinqToolStripMenuItem";
            this.testLinqToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.testLinqToolStripMenuItem.Text = "TestLinq";
            this.testLinqToolStripMenuItem.Click += new System.EventHandler(this.testLinqToolStripMenuItem_Click);
            // 
            // listeToolStripMenuItem1
            // 
            this.listeToolStripMenuItem1.Name = "listeToolStripMenuItem1";
            this.listeToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.listeToolStripMenuItem1.Text = "liste";
            this.listeToolStripMenuItem1.Click += new System.EventHandler(this.listeToolStripMenuItem1_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 262);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMenu";
            this.Text = "Gestion des leçons de conduite";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eleveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gérerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem véhiculeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gérerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem leçonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gérerToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem listeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validerTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validerTest2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validerTest3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validerTest4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testLinqToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeToolStripMenuItem1;
    }
}

