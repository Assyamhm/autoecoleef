﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class frmListeLecon : Form
    {
        private autoecoleEntities autoecoleEntities;

        public frmListeLecon()
        {
            InitializeComponent();
        }

        public frmListeLecon(autoecoleEntities autoecoleEntities)
        {
            InitializeComponent();
            this.autoecoleEntities = autoecoleEntities;
            


            this.bdgLecon.DataSource = this.autoecoleEntities.lecons;
            this.bdgEleve.DataSource = this.autoecoleEntities.eleves.Include("lecons");
        }
    }
}
