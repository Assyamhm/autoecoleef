﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmListeVehicule : Form
    {
        private autoecoleEntities mesDonnesEF;
        public FrmListeVehicule( autoecoleEntities mesDonnesEF)
        {
            InitializeComponent();
            this.mesDonnesEF = mesDonnesEF;
            this.bdgVehicules.DataSource = this.mesDonnesEF.vehicules;
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            this.bdgVehicules.EndEdit();
            this.mesDonnesEF.SaveChanges();
        }
    }
}
