﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmvaliderLeconTest3 : Form
    {
        private autoecoleEntities mesDonnesEF;
        public FrmvaliderLeconTest3(autoecoleEntities m)
        {
            InitializeComponent();
            this.mesDonnesEF = m;
            this.bdgsEleve.DataSource = this.mesDonnesEF.eleves.Include("lecons");
            this.bdgSLecon.DataSource = this.mesDonnesEF.lecons;
            this.bdgSvehicule.DataSource = this.mesDonnesEF.vehicules;
        }
    }
}
