﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    
    public partial class FrmvaliderLeconTest4 : Form
    {
        private autoecoleEntities mesDonnesEF;
        public FrmvaliderLeconTest4(autoecoleEntities mesdonnees)
        {
            InitializeComponent();
            this.mesDonnesEF = mesdonnees;
            //var lesLecons =( from l in mesDonnesEF.lecons
            //                where l.effectueeOui_Non == false
            //                select l);
            bdgSourceLecon.DataSource = this.mesDonnesEF.lecons.Include("eleve");
            this.eleveDataGridViewTextBoxColumn.DataPropertyName = "eleve.nom";
           
        }
    }
}
