﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmValiderLecon : Form
    {
        private autoecoleEntities mesDonnesEF;
        public FrmValiderLecon(autoecoleEntities mesDonnesEF)
        {
            InitializeComponent();
            this.mesDonnesEF = mesDonnesEF;
            try
            {
                this.mesDonnesEF.newVehicule("123yt79", "renault", "bleue", 1);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            MessageBox.Show("");
           
            var lesLecons = from l in mesDonnesEF.lecons

                            select new
                                {
                                    l.vehicule ,
                                    l.date,
                                    l.heure,
                                    l.eleve.nom,
                                    l.effectueeOui_Non
                                };
        //    this.dtGridLecon.DataSource = this.mesDonnesEF.lecons.Include("eleve");
            this.dtGridLecon.DataSource = lesLecons;
            this.bdgSlecon.DataSource = mesDonnesEF.lecons;
        //    this.idEleveDataGridViewTextBoxColumn.DataPropertyName = "l.eleve.nom";
            this.bdgSvehicule.DataSource = this.mesDonnesEF.vehicules;
         //   this.dtGridLecon.DataSource = this.bdgSlecon;
        //    this.dtGridLecon.Columns.Remove("eleve");
         //   this.dtGridLecon.Columns.Remove("vehicule");
           

            //                   select l.date,
           // dtGridLecon.Columns.Add("eleve.nom", "nom");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.mesDonnesEF.SaveChanges();
        }
    }
}
