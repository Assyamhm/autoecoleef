﻿namespace autoEcoleEF
{
    partial class FrmValiderLeconTest2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bdgSeleve = new System.Windows.Forms.BindingSource(this.components);
            this.dtgViewEleves = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgViewLecons = new System.Windows.Forms.DataGridView();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectueeOui_Non = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.numImmaVehicule = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bdgSvehicule = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idEleveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dureeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectueeOuiNonDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.numImmaVehiculeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eleveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehiculeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdgSlecon = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnEnregister = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSeleve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgViewEleves)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgViewLecons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSvehicule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSlecon)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "numImmaVehicule";
            this.dataGridViewTextBoxColumn6.HeaderText = "numImmaVehicule";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // bdgSeleve
            // 
            this.bdgSeleve.DataSource = typeof(autoEcoleEF.eleve);
            // 
            // dtgViewEleves
            // 
            this.dtgViewEleves.AutoGenerateColumns = false;
            this.dtgViewEleves.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgViewEleves.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7});
            this.dtgViewEleves.DataSource = this.bdgSeleve;
            this.dtgViewEleves.Location = new System.Drawing.Point(43, 37);
            this.dtgViewEleves.Name = "dtgViewEleves";
            this.dtgViewEleves.Size = new System.Drawing.Size(347, 127);
            this.dtgViewEleves.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "nom";
            this.dataGridViewTextBoxColumn2.HeaderText = "nom";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "prenom";
            this.dataGridViewTextBoxColumn4.HeaderText = "prenom";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "creditHoraire";
            this.dataGridViewTextBoxColumn7.HeaderText = "creditHoraire";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dtgViewLecons
            // 
            this.dtgViewLecons.AutoGenerateColumns = false;
            this.dtgViewLecons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgViewLecons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.date,
            this.heure,
            this.effectueeOui_Non,
            this.numImmaVehicule,
            this.idDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.idEleveDataGridViewTextBoxColumn,
            this.heureDataGridViewTextBoxColumn,
            this.dureeDataGridViewTextBoxColumn,
            this.effectueeOuiNonDataGridViewCheckBoxColumn,
            this.numImmaVehiculeDataGridViewTextBoxColumn,
            this.eleveDataGridViewTextBoxColumn,
            this.vehiculeDataGridViewTextBoxColumn});
            this.dtgViewLecons.DataBindings.Add(new System.Windows.Forms.Binding("DataSource", this.bdgSeleve, "lecons", true));
            this.dtgViewLecons.DataSource = this.bdgSlecon;
            this.dtgViewLecons.Location = new System.Drawing.Point(12, 212);
            this.dtgViewLecons.Name = "dtgViewLecons";
            this.dtgViewLecons.Size = new System.Drawing.Size(551, 131);
            this.dtgViewLecons.TabIndex = 1;
            // 
            // date
            // 
            this.date.DataPropertyName = "date";
            this.date.HeaderText = "date";
            this.date.Name = "date";
            // 
            // heure
            // 
            this.heure.DataPropertyName = "heure";
            this.heure.HeaderText = "heure";
            this.heure.Name = "heure";
            // 
            // effectueeOui_Non
            // 
            this.effectueeOui_Non.DataPropertyName = "effectueeOui_Non";
            this.effectueeOui_Non.HeaderText = "effectueeOui_Non";
            this.effectueeOui_Non.Name = "effectueeOui_Non";
            // 
            // numImmaVehicule
            // 
            this.numImmaVehicule.DataPropertyName = "numImmaVehicule";
            this.numImmaVehicule.DataSource = this.bdgSvehicule;
            this.numImmaVehicule.DisplayMember = "numImma";
            this.numImmaVehicule.HeaderText = "numImmaVehicule";
            this.numImmaVehicule.Name = "numImmaVehicule";
            this.numImmaVehicule.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.numImmaVehicule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // bdgSvehicule
            // 
            this.bdgSvehicule.DataSource = typeof(autoEcoleEF.vehicule);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            // 
            // idEleveDataGridViewTextBoxColumn
            // 
            this.idEleveDataGridViewTextBoxColumn.DataPropertyName = "idEleve";
            this.idEleveDataGridViewTextBoxColumn.HeaderText = "idEleve";
            this.idEleveDataGridViewTextBoxColumn.Name = "idEleveDataGridViewTextBoxColumn";
            // 
            // heureDataGridViewTextBoxColumn
            // 
            this.heureDataGridViewTextBoxColumn.DataPropertyName = "heure";
            this.heureDataGridViewTextBoxColumn.HeaderText = "heure";
            this.heureDataGridViewTextBoxColumn.Name = "heureDataGridViewTextBoxColumn";
            // 
            // dureeDataGridViewTextBoxColumn
            // 
            this.dureeDataGridViewTextBoxColumn.DataPropertyName = "duree";
            this.dureeDataGridViewTextBoxColumn.HeaderText = "duree";
            this.dureeDataGridViewTextBoxColumn.Name = "dureeDataGridViewTextBoxColumn";
            // 
            // effectueeOuiNonDataGridViewCheckBoxColumn
            // 
            this.effectueeOuiNonDataGridViewCheckBoxColumn.DataPropertyName = "effectueeOui_Non";
            this.effectueeOuiNonDataGridViewCheckBoxColumn.HeaderText = "effectueeOui_Non";
            this.effectueeOuiNonDataGridViewCheckBoxColumn.Name = "effectueeOuiNonDataGridViewCheckBoxColumn";
            // 
            // numImmaVehiculeDataGridViewTextBoxColumn
            // 
            this.numImmaVehiculeDataGridViewTextBoxColumn.DataPropertyName = "numImmaVehicule";
            this.numImmaVehiculeDataGridViewTextBoxColumn.HeaderText = "numImmaVehicule";
            this.numImmaVehiculeDataGridViewTextBoxColumn.Name = "numImmaVehiculeDataGridViewTextBoxColumn";
            // 
            // eleveDataGridViewTextBoxColumn
            // 
            this.eleveDataGridViewTextBoxColumn.DataPropertyName = "eleve";
            this.eleveDataGridViewTextBoxColumn.HeaderText = "eleve";
            this.eleveDataGridViewTextBoxColumn.Name = "eleveDataGridViewTextBoxColumn";
            // 
            // vehiculeDataGridViewTextBoxColumn
            // 
            this.vehiculeDataGridViewTextBoxColumn.DataPropertyName = "vehicule";
            this.vehiculeDataGridViewTextBoxColumn.HeaderText = "vehicule";
            this.vehiculeDataGridViewTextBoxColumn.Name = "vehiculeDataGridViewTextBoxColumn";
            // 
            // bdgSlecon
            // 
            this.bdgSlecon.DataSource = typeof(autoEcoleEF.lecon);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(203, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Liste des élèves";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(215, 192);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Les leçons de chaque élève";
            // 
            // btnEnregister
            // 
            this.btnEnregister.Location = new System.Drawing.Point(218, 349);
            this.btnEnregister.Name = "btnEnregister";
            this.btnEnregister.Size = new System.Drawing.Size(75, 23);
            this.btnEnregister.TabIndex = 4;
            this.btnEnregister.Text = "Enregistrer";
            this.btnEnregister.UseVisualStyleBackColor = true;
            this.btnEnregister.Click += new System.EventHandler(this.btnEnregister_Click);
            // 
            // FrmValiderLeconTest2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 391);
            this.Controls.Add(this.btnEnregister);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgViewLecons);
            this.Controls.Add(this.dtgViewEleves);
            this.Name = "FrmValiderLeconTest2";
            this.Text = "FrmValiderLeconTest2";
            ((System.ComponentModel.ISupportInitialize)(this.bdgSeleve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgViewEleves)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgViewLecons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSvehicule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSlecon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.BindingSource bdgSeleve;
        private System.Windows.Forms.DataGridView dtgViewEleves;
        private System.Windows.Forms.DataGridView dtgViewLecons;
        private System.Windows.Forms.BindingSource bdgSlecon;
        private System.Windows.Forms.BindingSource bdgSvehicule;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnEnregister;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn heure;
        private System.Windows.Forms.DataGridViewCheckBoxColumn effectueeOui_Non;
        private System.Windows.Forms.DataGridViewComboBoxColumn numImmaVehicule;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEleveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn heureDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dureeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn effectueeOuiNonDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numImmaVehiculeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eleveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehiculeDataGridViewTextBoxColumn;
    }
}