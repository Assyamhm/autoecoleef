﻿namespace autoEcoleEF
{
    partial class FrmAjoutLecon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.cmbEleve = new System.Windows.Forms.ComboBox();
            this.bdgsEleve = new System.Windows.Forms.BindingSource(this.components);
            this.dtLecon = new System.Windows.Forms.DateTimePicker();
            this.cmbHeure = new System.Windows.Forms.ComboBox();
            this.rdButton1 = new System.Windows.Forms.RadioButton();
            this.rdButton2 = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.bdgsEleve)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Elève";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Heure";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Durée";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(58, 217);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(100, 29);
            this.btnEnregistrer.TabIndex = 4;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // cmbEleve
            // 
            this.cmbEleve.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdgsEleve, "nom", true));
            this.cmbEleve.DataSource = this.bdgsEleve;
            this.cmbEleve.DisplayMember = "nom";
            this.cmbEleve.FormattingEnabled = true;
            this.cmbEleve.Location = new System.Drawing.Point(113, 22);
            this.cmbEleve.Name = "cmbEleve";
            this.cmbEleve.Size = new System.Drawing.Size(121, 21);
            this.cmbEleve.TabIndex = 5;
            this.cmbEleve.SelectedIndexChanged += new System.EventHandler(this.cmbEleve_SelectedIndexChanged);
            // 
            // bdgsEleve
            // 
            this.bdgsEleve.DataSource = typeof(autoEcoleEF.eleve);
            // 
            // dtLecon
            // 
            this.dtLecon.Location = new System.Drawing.Point(113, 64);
            this.dtLecon.Name = "dtLecon";
            this.dtLecon.Size = new System.Drawing.Size(200, 20);
            this.dtLecon.TabIndex = 6;
            // 
            // cmbHeure
            // 
            this.cmbHeure.FormattingEnabled = true;
            this.cmbHeure.Location = new System.Drawing.Point(113, 107);
            this.cmbHeure.Name = "cmbHeure";
            this.cmbHeure.Size = new System.Drawing.Size(121, 21);
            this.cmbHeure.TabIndex = 7;
            // 
            // rdButton1
            // 
            this.rdButton1.AutoSize = true;
            this.rdButton1.Checked = true;
            this.rdButton1.Location = new System.Drawing.Point(113, 153);
            this.rdButton1.Name = "rdButton1";
            this.rdButton1.Size = new System.Drawing.Size(75, 17);
            this.rdButton1.TabIndex = 8;
            this.rdButton1.TabStop = true;
            this.rdButton1.Text = "Une heure";
            this.rdButton1.UseVisualStyleBackColor = true;
            // 
            // rdButton2
            // 
            this.rdButton2.AutoSize = true;
            this.rdButton2.Location = new System.Drawing.Point(216, 153);
            this.rdButton2.Name = "rdButton2";
            this.rdButton2.Size = new System.Drawing.Size(85, 17);
            this.rdButton2.TabIndex = 9;
            this.rdButton2.TabStop = true;
            this.rdButton2.Text = "Deux heures";
            this.rdButton2.UseVisualStyleBackColor = true;
            // 
            // FrmAjoutLecon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 262);
            this.Controls.Add(this.rdButton2);
            this.Controls.Add(this.rdButton1);
            this.Controls.Add(this.cmbHeure);
            this.Controls.Add(this.dtLecon);
            this.Controls.Add(this.cmbEleve);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmAjoutLecon";
            this.Text = "Ajouter une leçon";
            ((System.ComponentModel.ISupportInitialize)(this.bdgsEleve)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.ComboBox cmbEleve;
        private System.Windows.Forms.DateTimePicker dtLecon;
        private System.Windows.Forms.ComboBox cmbHeure;
        private System.Windows.Forms.RadioButton rdButton1;
        private System.Windows.Forms.RadioButton rdButton2;
        private System.Windows.Forms.BindingSource bdgsEleve;
    }
}