﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcoleEF
{
    public partial class FrmAjoutLecon : Form
    {
        private autoecoleEntities mesDonnesEF;
        public FrmAjoutLecon(autoecoleEntities mesDonnesEF)
        {
            InitializeComponent();
            this.mesDonnesEF = mesDonnesEF;
            this.bdgsEleve.DataSource = this.mesDonnesEF.eleves;
            for (int i = 8; i < 20; i++)
                this.cmbHeure.Items.Add(i);
       
        }
        private int newNumLecon()
        {
            int n;
            int  dernier = (from ra in this.mesDonnesEF.lecons
                              select ra.id).Max();
            n = dernier + 1;
            return n;
        }
        private lecon nouvellelecon()
        {
            int n = newNumLecon();
            eleve el = (eleve)cmbEleve.SelectedValue;
            DateTime d = this.dtLecon.Value;
            int heureLecon = Convert.ToInt32(this.cmbHeure.SelectedItem);
            int nbHeures = 1;
            if (this.rdButton1.Checked)
                nbHeures = 2;
            lecon l = new lecon();
            l.date = d;
            l.heure = heureLecon;
            l.duree = nbHeures;
            l.id = n;
            l.eleve = el;
            return l;
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            this.mesDonnesEF.lecons.AddObject(nouvellelecon());
            this.mesDonnesEF.SaveChanges();
        }

        private void cmbEleve_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
