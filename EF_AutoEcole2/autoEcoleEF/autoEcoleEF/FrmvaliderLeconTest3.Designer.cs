﻿namespace autoEcoleEF
{
    partial class FrmvaliderLeconTest3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdgsEleve = new System.Windows.Forms.BindingSource(this.components);
            this.bdgSLecon = new System.Windows.Forms.BindingSource(this.components);
            this.bdgSvehicule = new System.Windows.Forms.BindingSource(this.components);
            this.dtGEleve = new System.Windows.Forms.DataGridView();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creditHoraireDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgViewLecon = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idEleveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dureeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectueeOuiNonDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.numImmaVehiculeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eleveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehiculeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdgsEleve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSLecon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSvehicule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGEleve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgViewLecon)).BeginInit();
            this.SuspendLayout();
            // 
            // bdgsEleve
            // 
            this.bdgsEleve.DataSource = typeof(autoEcoleEF.eleve);
            // 
            // bdgSLecon
            // 
            this.bdgSLecon.DataSource = typeof(autoEcoleEF.lecon);
            // 
            // bdgSvehicule
            // 
            this.bdgSvehicule.DataSource = typeof(autoEcoleEF.vehicule);
            // 
            // dtGEleve
            // 
            this.dtGEleve.AutoGenerateColumns = false;
            this.dtGEleve.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGEleve.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomDataGridViewTextBoxColumn,
            this.prenomDataGridViewTextBoxColumn,
            this.creditHoraireDataGridViewTextBoxColumn});
            this.dtGEleve.DataSource = this.bdgsEleve;
            this.dtGEleve.Location = new System.Drawing.Point(12, 12);
            this.dtGEleve.Name = "dtGEleve";
            this.dtGEleve.Size = new System.Drawing.Size(240, 150);
            this.dtGEleve.TabIndex = 0;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            this.nomDataGridViewTextBoxColumn.HeaderText = "nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            // 
            // prenomDataGridViewTextBoxColumn
            // 
            this.prenomDataGridViewTextBoxColumn.DataPropertyName = "prenom";
            this.prenomDataGridViewTextBoxColumn.HeaderText = "prenom";
            this.prenomDataGridViewTextBoxColumn.Name = "prenomDataGridViewTextBoxColumn";
            // 
            // creditHoraireDataGridViewTextBoxColumn
            // 
            this.creditHoraireDataGridViewTextBoxColumn.DataPropertyName = "creditHoraire";
            this.creditHoraireDataGridViewTextBoxColumn.HeaderText = "creditHoraire";
            this.creditHoraireDataGridViewTextBoxColumn.Name = "creditHoraireDataGridViewTextBoxColumn";
            // 
            // dtgViewLecon
            // 
            this.dtgViewLecon.AutoGenerateColumns = false;
            this.dtgViewLecon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgViewLecon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.idEleveDataGridViewTextBoxColumn,
            this.heureDataGridViewTextBoxColumn,
            this.dureeDataGridViewTextBoxColumn,
            this.effectueeOuiNonDataGridViewCheckBoxColumn,
            this.numImmaVehiculeDataGridViewTextBoxColumn,
            this.eleveDataGridViewTextBoxColumn,
            this.vehiculeDataGridViewTextBoxColumn});
            this.dtgViewLecon.DataSource = this.bdgSLecon;
            this.dtgViewLecon.Location = new System.Drawing.Point(2, 192);
            this.dtgViewLecon.Name = "dtgViewLecon";
            this.dtgViewLecon.Size = new System.Drawing.Size(374, 69);
            this.dtgViewLecon.TabIndex = 1;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            // 
            // idEleveDataGridViewTextBoxColumn
            // 
            this.idEleveDataGridViewTextBoxColumn.DataPropertyName = "idEleve";
            this.idEleveDataGridViewTextBoxColumn.HeaderText = "idEleve";
            this.idEleveDataGridViewTextBoxColumn.Name = "idEleveDataGridViewTextBoxColumn";
            // 
            // heureDataGridViewTextBoxColumn
            // 
            this.heureDataGridViewTextBoxColumn.DataPropertyName = "heure";
            this.heureDataGridViewTextBoxColumn.HeaderText = "heure";
            this.heureDataGridViewTextBoxColumn.Name = "heureDataGridViewTextBoxColumn";
            // 
            // dureeDataGridViewTextBoxColumn
            // 
            this.dureeDataGridViewTextBoxColumn.DataPropertyName = "duree";
            this.dureeDataGridViewTextBoxColumn.HeaderText = "duree";
            this.dureeDataGridViewTextBoxColumn.Name = "dureeDataGridViewTextBoxColumn";
            // 
            // effectueeOuiNonDataGridViewCheckBoxColumn
            // 
            this.effectueeOuiNonDataGridViewCheckBoxColumn.DataPropertyName = "effectueeOui_Non";
            this.effectueeOuiNonDataGridViewCheckBoxColumn.HeaderText = "effectueeOui_Non";
            this.effectueeOuiNonDataGridViewCheckBoxColumn.Name = "effectueeOuiNonDataGridViewCheckBoxColumn";
            // 
            // numImmaVehiculeDataGridViewTextBoxColumn
            // 
            this.numImmaVehiculeDataGridViewTextBoxColumn.DataPropertyName = "numImmaVehicule";
            this.numImmaVehiculeDataGridViewTextBoxColumn.HeaderText = "numImmaVehicule";
            this.numImmaVehiculeDataGridViewTextBoxColumn.Name = "numImmaVehiculeDataGridViewTextBoxColumn";
            // 
            // eleveDataGridViewTextBoxColumn
            // 
            this.eleveDataGridViewTextBoxColumn.DataPropertyName = "eleve";
            this.eleveDataGridViewTextBoxColumn.HeaderText = "eleve";
            this.eleveDataGridViewTextBoxColumn.Name = "eleveDataGridViewTextBoxColumn";
            // 
            // vehiculeDataGridViewTextBoxColumn
            // 
            this.vehiculeDataGridViewTextBoxColumn.DataPropertyName = "vehicule";
            this.vehiculeDataGridViewTextBoxColumn.HeaderText = "vehicule";
            this.vehiculeDataGridViewTextBoxColumn.Name = "vehiculeDataGridViewTextBoxColumn";
            // 
            // FrmvaliderLeconTest3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 365);
            this.Controls.Add(this.dtgViewLecon);
            this.Controls.Add(this.dtGEleve);
            this.Name = "FrmvaliderLeconTest3";
            this.Text = "FrmvaliderLeconTest3";
            ((System.ComponentModel.ISupportInitialize)(this.bdgsEleve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSLecon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdgSvehicule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGEleve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgViewLecon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bdgsEleve;
        private System.Windows.Forms.BindingSource bdgSLecon;
        private System.Windows.Forms.BindingSource bdgSvehicule;
        private System.Windows.Forms.DataGridView dtGEleve;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn creditHoraireDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dtgViewLecon;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEleveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn heureDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dureeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn effectueeOuiNonDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numImmaVehiculeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eleveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehiculeDataGridViewTextBoxColumn;
    }
}