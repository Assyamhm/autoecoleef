﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoEcole
{
    public partial class FrmEleve : Form
    {
        private AutoEcoleEntities mesDonnees;
        public FrmEleve(AutoEcoleEntities ef)
        {
            InitializeComponent();
            this.mesDonnees = ef;
            this.bdgEleve.DataSource = mesDonnees.eleves;
         

            for (int i = 1; i < 45; i++)
                this.cbxHoraire.Items.Add(i);

          
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //this.mesDonnees.SaveChanges();
        }
        private int getNumero()
        {
            
            var num = (from el in this.mesDonnees.eleves
                       orderby el.id descending
                       select el
                           ).First().id;
            
           
          

            
            return num + 1;
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            this.txtNum.Text = this.getNumero().ToString();
        }

        private void bindingNavigatorAddNewItem_CheckedChanged(object sender, EventArgs e)
        {
            //if(this.txtNom.Text =="" || this.txtAdresse.Text=="" || txtPrenom.Text=="" )
            //    MessageBox.Show("Il faut saisir tous les champs");
            //bdgEleve.CancelEdit();
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (this.txtNom.Text == "" || this.txtAdresse.Text == "" || txtPrenom.Text == "")
                MessageBox.Show("Il faut saisir tous les champs");
           // bdgEleve.CancelEdit();

        }

        private void bindingNavigatorAddNewItem_CheckStateChanged(object sender, EventArgs e)
        {
            if (this.txtNom.Text == "" || this.txtAdresse.Text == "" || txtPrenom.Text == "")
                MessageBox.Show("Il faut saisir tous les champs");
           // bdgEleve.CancelEdit();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
