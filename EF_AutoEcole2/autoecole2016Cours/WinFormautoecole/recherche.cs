﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
 

namespace AutoEcole
{
    public partial class recherche : Form
    {
        private AutoEcoleEntities mesDonnees;
        public recherche(AutoEcoleEntities mesDonnees)
        {
            this.mesDonnees = mesDonnees;
            InitializeComponent();
        }

        private void txtNom_TextChanged(object sender, EventArgs e)
        {
            var req = from el in this.mesDonnees.eleves
                      where el.nom..StartsWith(this.txtNom.Text)
                      select el.nom ;
            this.lstEleves.DataSource = req;
           
        }
    }
}
