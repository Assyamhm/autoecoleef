﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmAccueil : Form
    {
        private autoecoleEntities1 mesDonnees;
        public FrmAccueil()
        {
            InitializeComponent();
            this.mesDonnees = new autoecoleEntities1();
  
            
        }

        private void gérerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEleve f = new FrmEleve(this.mesDonnees);
            f.Show();
        }

        private void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmListeVehicules f = new FrmListeVehicules(this.mesDonnees);
            f.Show();
        }

        private void gérerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmVehicule f = new FrmVehicule(this.mesDonnees);
            f.Show();
        }

       

       
    }
}
