﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmVehicule : Form
    {
        private autoecoleEntities1 mesDonnees;
        public FrmVehicule(autoecoleEntities1 e)
        {
            InitializeComponent();
            this.mesDonnees = e;
            this.bdgSourceVehicule.DataSource = this.mesDonnees.vehicules;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            
            this.mesDonnees.SaveChanges();
        }
    }
}
