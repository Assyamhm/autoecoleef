﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmListeVehicules : Form
    {
        private autoecoleEntities1 mesDonnees;
        public FrmListeVehicules( autoecoleEntities1 d)
        {
            InitializeComponent();
            this.mesDonnees = d;
            this.bdgVehicule.DataSource = this.mesDonnees.vehicules;
        }
    }
}
