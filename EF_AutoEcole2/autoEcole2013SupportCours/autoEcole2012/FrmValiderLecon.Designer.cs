﻿namespace autoEcole2012
{
    partial class FrmValiderLecon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idEleve = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numImmaVehicule = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idEleve,
            this.numImmaVehicule});
            this.dataGridView1.Location = new System.Drawing.Point(-7, 72);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(620, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // idEleve
            // 
            this.idEleve.DataPropertyName = "idEleve";
            this.idEleve.HeaderText = "idEleve";
            this.idEleve.Name = "idEleve";
            // 
            // numImmaVehicule
            // 
            this.numImmaVehicule.DataPropertyName = "numImmaVehicule";
            this.numImmaVehicule.HeaderText = "numImmaVehicule";
            this.numImmaVehicule.Name = "numImmaVehicule";
            // 
            // FrmValiderLecon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 262);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FrmValiderLecon";
            this.Text = "FrmValiderLecon";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
    
        private System.Windows.Forms.DataGridViewTextBoxColumn idEleve;
        private System.Windows.Forms.DataGridViewComboBoxColumn numImmaVehicule;
    }
}