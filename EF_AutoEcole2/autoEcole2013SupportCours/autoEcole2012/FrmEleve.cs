﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace autoEcole2012
{
    public partial class FrmEleve : Form
    {
        private autoecoleEntities1 mesDonnees;
        public FrmEleve(autoecoleEntities1 ef)
        {
            InitializeComponent();
            this.mesDonnees = ef;
          

            for (int i = 1; i < 45; i++)
                this.cbxHoraire.Items.Add(i);

           this.bdgEleve.DataSource = this.mesDonnees.eleves;
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.mesDonnees.SaveChanges();
        }
        private int getNumero()
        {
            int n = 0;
            var req = (from el in this.mesDonnees.eleves
                       orderby el.id descending
                       select el
                           );
            eleve dernier = req.First();
           n = dernier.id;
            n = n+1;
            return n;
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            this.txtNum.Text = this.getNumero().ToString();
        }

        private void bindingNavigatorAddNewItem_CheckedChanged(object sender, EventArgs e)
        {
            //if(this.txtNom.Text =="" || this.txtAdresse.Text=="" || txtPrenom.Text=="" )
            //    MessageBox.Show("Il faut saisir tous les champs");
            //bdgEleve.CancelEdit();
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (this.txtNom.Text == "" || this.txtAdresse.Text == "" || txtPrenom.Text == "")
                MessageBox.Show("Il faut saisir tous les champs");
            bdgEleve.CancelEdit();

        }

        private void bindingNavigatorAddNewItem_CheckStateChanged(object sender, EventArgs e)
        {
            if (this.txtNom.Text == "" || this.txtAdresse.Text == "" || txtPrenom.Text == "")
                MessageBox.Show("Il faut saisir tous les champs");
            bdgEleve.CancelEdit();
        }
    }
}
