﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace autoEcoleVS2015
{
    public partial class FrmEleve : Form
    {
        autoecoleEntities mesDonnees;
        public FrmEleve(autoecoleEntities mesDonnees)
        {
            InitializeComponent();
            //for (int i = 1; i < 50; i++)
            //    this.cbxCredits.Items.Add(i);
            this.mesDonnees = mesDonnees;
            this.mesDonnees.eleves.Load();

            bdgEleve.DataSource = this.mesDonnees.eleves.Local.ToBindingList();
        //    this.cbxCredits.DataSource = this.bdgEleve;
        //    this.cbxCredits.ValueMember = "creditHoraire";
         //   this.cbxCredits.DisplayMember = "creditHoraire";


        }
        private int getNumero()
        {
            int n = 0;
            var req = (from el in this.mesDonnees.eleves
                       orderby el.id descending
                       select el
                           );
            eleve dernier = req.First();
            n = dernier.id;

            n = n + 1;
            return n;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.bdgEleve.EndEdit();
            this.mesDonnees.SaveChanges();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            this.txtNumero.Text = this.getNumero().ToString();
            this.bdgEleve.EndEdit();
        }
    }
}
